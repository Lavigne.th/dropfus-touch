import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appAccordion]'
})
export class AccordionDirective implements OnInit {

  public isActive: boolean = false;

  constructor(private elementRef: ElementRef) {

  }

  ngOnInit(): void {
    this.elementRef.nativeElement.nextElementSibling.classList.add('hidden');
  }

  @HostListener('click', ['$event'])
  onClick($event: MouseEvent): void {
    this.isActive = !this.isActive;
    if (this.isActive === true) {
      this.elementRef.nativeElement.nextElementSibling.classList.remove('hidden');
    } else {
      this.elementRef.nativeElement.nextElementSibling.classList.add('hidden');
    }
  }
}
