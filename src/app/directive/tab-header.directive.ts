import { AfterContentInit, Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appTabHeader]'
})
export class TabHeaderDirective implements AfterContentInit {

  @Input() groupName: string = '';

  @Input() targetTabName: string = '';

  constructor(private el: ElementRef) { }

  ngAfterContentInit(): void {

  }

  @HostListener('click', ['$event'])
  onHeaderClicked(evt: MouseEvent): void {
    this.setThisHeaderAsActive();
    this.setThisContentAsActive();
  }

  private setThisContentAsActive(): void {
    document.querySelectorAll(`[group-name="${this.groupName}"][tab-name]`).forEach((elem: Element) => {
      if (elem.getAttribute('tab-name') === this.targetTabName) {
        if (elem.classList.contains('active') === false) {
          elem.classList.add('active')
        }
      } else if (elem.classList.contains('active') === true) {
        elem.classList.remove('active')
      }
    });
  }

  private setThisHeaderAsActive(): void {
    document.querySelectorAll(`[apptabheader][groupname="${this.groupName}"]`).forEach((elem: Element) => {
      if (elem.getAttribute('targettabname') === this.targetTabName) {
        if (elem.classList.contains('active') === false) {
          elem.classList.add('active')
        }
      } else if (elem.classList.contains('active') === true) {
        elem.classList.remove('active')
      }
    });
  }
}
