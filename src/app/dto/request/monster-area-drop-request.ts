import { DROP_REQUEST_TYPE } from "../enums/drop-request-type-enum";

export class MonsterAreaDropRequest {
    public dropType: DROP_REQUEST_TYPE;

    public forbiddenSubAreas: Array<number>;

    public forbiddenMonsters: Array<number>;

    public includeBosses: boolean;

    public includeMiniBosses: boolean;

    public includeQuestMonster: boolean;

    public includeBestHormonePrice: boolean;

    public includeHunterDrops: boolean;

    public page: number;

    constructor(
        requestType: DROP_REQUEST_TYPE,
        includeBoss: boolean,
        includeMiniBoss: boolean,
        includeQuestMonster: boolean,
        includeBestHormone: boolean,
        includeHunterDrops: boolean,
        forbiddenAreas: Array<number>,
        forbiddenMonsters: Array<number>,
        page: number
    ) {
        this.dropType = requestType
        this.includeBosses = includeBoss
        this.includeMiniBosses = includeMiniBoss
        this.includeQuestMonster = includeQuestMonster
        this.includeBestHormonePrice = includeBestHormone
        this.includeHunterDrops = includeHunterDrops
        this.forbiddenSubAreas = forbiddenAreas
        this.forbiddenMonsters = forbiddenMonsters;
        this.page = page;
    }
}