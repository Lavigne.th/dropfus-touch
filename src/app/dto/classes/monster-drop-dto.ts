export class MonsterDropDto {
    public itemId: number;

    public probability: number;

    public itemName: string;

    public perFightValue: number;

    public avgPrice: number;

    public includeDropInMonsterValue: boolean;

    constructor(
        itemId: number,
        probability: number,
        itemName: string,
        perFightValue: number,
        avgPrice: number,
        includeDropInMonsterValue: boolean
    ) {
        this.itemId = itemId
        this.probability = probability
        this.itemName = itemName
        this.perFightValue = perFightValue
        this.avgPrice = avgPrice
        this.includeDropInMonsterValue = includeDropInMonsterValue
    }
}