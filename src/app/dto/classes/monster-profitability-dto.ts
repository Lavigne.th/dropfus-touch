import { MonsterDropDto } from "./monster-drop-dto";

export class MonsterProfitabilityDto {
    public monsterId: number;

    public monsterName: string;

    public subAreaId: number;

    public averageDropValue: number;

    public drops: Array<MonsterDropDto>;

    public appearsInAreaIds: Array<number>;

    constructor(
        monsterId: number,
        monsterName: string,
        subAreaId: number,
        averageDropValue: number,
        drops: Array<MonsterDropDto>,
        appearsInAreaIds: Array<number>
    ) {
        this.monsterId = monsterId
        this.monsterName = monsterName
        this.subAreaId = subAreaId
        this.averageDropValue = averageDropValue
        this.drops = drops
        this.appearsInAreaIds = appearsInAreaIds
    }
}