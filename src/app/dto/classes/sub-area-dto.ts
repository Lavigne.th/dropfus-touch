export class SubAreaDto {
    public id: number;
    public dofusId: number;
    public areaId: number
    public name: string;

    constructor(
        id: number,
        dofusId: number,
        areaId: number,
        name: string
    ) {
        this.id = id
        this.dofusId = dofusId
        this.areaId = areaId
        this.name = name
    }
}