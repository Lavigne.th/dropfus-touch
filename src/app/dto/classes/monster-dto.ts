export class MonsterDto {

    public id: number;
    public dofusId: number;
    public name: string;
    public isBoss: boolean;
    public isMiniBoss: boolean;
    public isQuest: boolean;

    constructor(
        id: number,
        dofusId: number,
        name: string,
        isBoss: boolean,
        isMiniBoss: boolean,
        isQuest: boolean
    ) {
        this.id = id
        this.dofusId = dofusId
        this.name = name
        this.isBoss = isBoss
        this.isMiniBoss = isMiniBoss
        this.isQuest = isQuest
    }

}