import { MonsterProfitabilityDto } from "./monster-profitability-dto";

export class SubAreaProfitabilityDto {
    public subAreaId: number;

    public subAreaName: string;

    public averageDropValue: number;

    public monsters: Array<MonsterProfitabilityDto>;

    constructor(
        subAreaId: number,
        subAreaName: string,
        averageDropValue: number,
        monsters: Array<MonsterProfitabilityDto>
    ) {
        this.subAreaId = subAreaId
        this.subAreaName = subAreaName
        this.averageDropValue = averageDropValue
        this.monsters = monsters
    }

}