import { SubAreaDto } from "../classes/sub-area-dto";

export class AllSubAreaResponse {
  public areas: Array<SubAreaDto>;
  
  constructor(areas: Array<SubAreaDto>) {
    this.areas = areas
  }
}