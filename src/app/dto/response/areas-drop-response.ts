import { SubAreaProfitabilityDto } from "../classes/sub-area-profitability-dto";

export class AreaDropResponse {
    public areas: Array<SubAreaProfitabilityDto>;

    public totalResults: number;

    public currentTotalResuls: number;

    constructor(areas: Array<SubAreaProfitabilityDto>, totalResults: number, currentTotalResuls: number) {
        this.areas = areas
        this.totalResults = totalResults
        this.currentTotalResuls = currentTotalResuls
    }

}