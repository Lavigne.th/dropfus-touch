import { MonsterDto } from "../classes/monster-dto";

export class AllMonsterResponse {
    public monsters: Array<MonsterDto>;

    constructor(monsters: Array<MonsterDto>) {
        this.monsters = monsters
    }
}