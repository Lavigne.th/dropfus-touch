import { MonsterProfitabilityDto } from "../classes/monster-profitability-dto";

export class MonstersDropResponse {
    public monsters: Array<MonsterProfitabilityDto>;
    
    public totalResults: number;

    public currentTotalResuls: number;

    constructor(monsters: Array<MonsterProfitabilityDto>, totalResults: number, currentTotalResuls: number) {
        this.monsters = monsters
        this.totalResults = totalResults
        this.currentTotalResuls = currentTotalResuls
    }

}