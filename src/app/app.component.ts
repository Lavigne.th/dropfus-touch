import { Component, HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from './constant';
import { SUPPORTED_LANGUAGE } from './enums/supported-language-enum';
import { WINDOW } from './enums/window-enum';
import { DESKTOP_WINDOW_WIDTH } from './enums/window-width-enum';
import { LocalStorageService } from './service/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dropfus-touch';

  public DESKTOP_WINDOW_WIDTH_ENUM = DESKTOP_WINDOW_WIDTH;

  public WINDOW_ENUM = WINDOW;

  public height: number = 0;

  constructor(private translateService: TranslateService, private localStorage: LocalStorageService) {
    this.setCurrentLangage();
    this.setHeight();
  }

  public setCurrentLangage(): void {
    let language: string | null | undefined = this.localStorage.get(Constant.LOCAL_STORAGE_LANGUAGE_KEY);
    if(language === undefined || language === null) {
      this.translateService.use(SUPPORTED_LANGUAGE.FRENCH);
    } else {
      this.translateService.use(language);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.setHeight();
  }
  setHeight() {
    this.height = window.innerHeight;
  }
}
