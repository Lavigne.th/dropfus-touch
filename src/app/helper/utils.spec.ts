import { SCREEN_SIZE } from "../enums/screen-size-enum";
import { Utils } from "./utils";

describe('utils', () => {
  
    it('should create determine if the app is on mobile display mode', () => {
        expect(Utils.isMobileDisplayMode(400))?.toBeTrue();
        expect(Utils.isMobileDisplayMode(2000))?.toBeFalse();

        expect(Utils.isMobileDisplayMode(<number>SCREEN_SIZE.MD))?.toBeFalse();
        expect(Utils.isMobileDisplayMode(<number>SCREEN_SIZE.MD - 1))?.toBeTrue();
    });
  });