import { SCREEN_SIZE } from "../enums/screen-size-enum";

export class Utils {
    public static remToPixels(rem: number) {    
        return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
    }

    public static isMobileDisplayMode(windowWidth: number): boolean {
        return windowWidth < <number>SCREEN_SIZE.MD;
    }

    public static numberWithSpaces(x: number): string {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
}

export async function sleep(ms: number): Promise<any> {
    return new Promise( resolve => setTimeout(resolve, ms) );
}