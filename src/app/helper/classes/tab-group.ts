import { TabContentDirective } from "src/app/directive/tab-content.directive";
import { TabHeaderDirective } from "src/app/directive/tab-header.directive";

export class TabGroup {
    name: string;

    headers: Array<TabHeaderDirective> = [];

    contents: Array<TabContentDirective> = [];

    constructor(name: string) {
        this.name = name
    }

    tryRemoveHeader(header: TabHeaderDirective): boolean {
        return true;
    }

    tryRemoveContent(content: TabContentDirective): boolean {
        return true;
    }

    isEmpty(): boolean {
        return true;
    }

}