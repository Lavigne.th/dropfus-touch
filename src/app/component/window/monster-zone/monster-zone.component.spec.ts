import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { MonsterAreaDropResultService } from 'src/app/service/monster-area-drop-result.service';

import { MonsterZoneComponent } from './monster-zone.component';

describe('MonsterZoneComponent', () => {
  let component: MonsterZoneComponent;
  let fixture: ComponentFixture<MonsterZoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot(), HttpClientTestingModule],
      declarations: [ MonsterZoneComponent ],
      providers: [ MonsterAreaDropResultService ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonsterZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('show mobile results', () => {
    component.showMobileResults();
    expect(component.showResultsPanel).toBeTrue();
    expect(component.showSearchPanel).toBeFalse();
    expect(component.windowService.showBackBtn).toBeTrue();
  });

  it('hide mobile results', () => {
    component.hideMobileResults();
    expect(component.showResultsPanel).toBeFalse();
    expect(component.showSearchPanel).toBeTrue();
    expect(component.windowService.showBackBtn).toBeFalse();
  });

  it('set desktop ui', () => {
    component.setDesktopUI();
    expect(component.showResultsPanel).toBeTrue();
    expect(component.showSearchPanel).toBeTrue();
    expect(component.windowService.showBackBtn).toBeFalse();
  });
});
