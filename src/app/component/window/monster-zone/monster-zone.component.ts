import { AfterContentInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { MonsterAreaDropRequest } from 'src/app/dto/request/monster-area-drop-request';
import { WINDOW } from 'src/app/enums/window-enum';
import { Utils } from 'src/app/helper/utils';
import { MenuService } from 'src/app/service/menu.service';
import { MonsterAreaDropResultService } from 'src/app/service/monster-area-drop-result.service';
import { WindowService } from 'src/app/service/window.service';

@Component({
  selector: 'app-monster-zone',
  templateUrl: './monster-zone.component.html',
  styleUrls: ['./monster-zone.component.css']
})
export class MonsterZoneComponent implements OnInit, AfterContentInit, OnDestroy {

  public maxContentHeight: number = 0;

  public showSearchPanel: boolean = true;

  public showResultsPanel: boolean = true;

  private backBtnClickedSub?: Subscription;

  constructor(public windowService: WindowService, private menuService: MenuService, public maDropRequest: MonsterAreaDropResultService) { 
  }

  ngOnDestroy(): void {
    this.backBtnClickedSub?.unsubscribe();
  }


  ngOnInit(): void {
    this.backBtnClickedSub = this.windowService.backButtonClicked.subscribe(() => {
      if(Utils.isMobileDisplayMode(window.innerWidth) === true) {
        this.hideMobileResults();
      }
    });
    if(Utils.isMobileDisplayMode(window.innerWidth) === true) {
      this.hideMobileResults();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.processMaxHeight();
    if(Utils.isMobileDisplayMode(window.innerWidth) === false) {
      this.setDesktopUI();
    } else {
      if(this.maDropRequest.isLoading === true || this.maDropRequest.hasResults() === true) {
        this.showMobileResults();
      } else {
        this.hideMobileResults();
      }
    }
  }

  onScroll(evt: any): void {
    if (evt.target.offsetHeight + evt.target.scrollTop >= evt.target.scrollHeight) {
      this.maDropRequest.loadNextPage();
    }
  }

  ngAfterContentInit(): void {
    this.processMaxHeight();

  }

  onSearchDropRequest(): void {
    if(Utils.isMobileDisplayMode(window.innerWidth) === true) {
      this.showMobileResults();
    }
    this.maDropRequest.getDrops();
  }

  showMobileResults(): void {
    this.showResultsPanel = true;
    this.showSearchPanel = false;
    this.windowService.showBackBtn = true;
  }

  hideMobileResults(): void {
    this.showResultsPanel = false;
    this.showSearchPanel = true;
    this.windowService.showBackBtn = false;
  }

  setDesktopUI(): void {
    this.showResultsPanel = true;
    this.showSearchPanel = true;
    this.windowService.showBackBtn = false;
  }

  /**
   * Calcul la hauteur maximale du contenu afin que la fenetre tienne dans la page sans scroll
   */
  processMaxHeight() {
    let mainPadding: number = 0;
    let mainHeight: number = 0;
    try {
      let mainElement: HTMLElement = <HTMLElement>document.querySelector('main');
      mainPadding = parseInt(getComputedStyle(mainElement)['padding'].replace('px', ''));
      mainHeight = mainElement.offsetHeight

    } catch (e) {
      mainPadding = 0;
      mainHeight = 0;
    }
    //remToPixels: 3 estimated size of the title of the window, 1.5 2 * padding of the container
    this.maxContentHeight = mainHeight - 2 * mainPadding - Utils.remToPixels(3) - Utils.remToPixels(1.5);
  }
}
