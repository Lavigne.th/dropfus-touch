import { Component, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputComponent } from 'src/app/component/templates/input/input.component';
import { MonsterFiltersService } from 'src/app/service/monster-filters.service';
import { FilteredMonsterComponent } from './filtered-monster/filtered-monster.component';

@Component({
  selector: '[app-monster-filters]',
  templateUrl: './monster-filters.component.html',
  styleUrls: ['./monster-filters.component.css']
})
export class MonsterFiltersComponent implements OnInit {

  @ViewChild("monsterNameInput") monsterNameInput?: InputComponent;

  private sub?: Subscription;

  @ViewChildren(FilteredMonsterComponent) filteredMonsters!: QueryList<FilteredMonsterComponent>;

  constructor(public monsterFilterService: MonsterFiltersService) { }

  ngAfterViewInit(): void {
    if(this.monsterNameInput) {
      this.monsterNameInput.value = this.monsterFilterService.monsterNameFilter;
      this.monsterNameInput.changeRef.detectChanges();
    }
    this.monsterNameInput?.valueChange.subscribe((newValue: string) => {
      this.monsterFilterService.setAreaNameFilter(newValue);
    });
  }

  ngOnInit(): void {
    this.monsterFilterService.loadMonsters();
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  selectAll(): void {
    this.monsterFilterService.selectAll();
    this.filteredMonsters.forEach((f: FilteredMonsterComponent) => {
      f.updateSelected();
    })
  }

  deSelectAll(): void {
    this.monsterFilterService.deSelectAll();
    this.filteredMonsters.forEach((f: FilteredMonsterComponent) => {
      f.updateSelected();
    })
  }
}
