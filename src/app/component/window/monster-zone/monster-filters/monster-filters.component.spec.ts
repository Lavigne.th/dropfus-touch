import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { InputComponent } from 'src/app/component/templates/input/input.component';

import { MonsterFiltersComponent } from './monster-filters.component';

describe('MonsterFiltersComponent', () => {
  let component: MonsterFiltersComponent;
  let fixture: ComponentFixture<MonsterFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(), 
        HttpClientTestingModule
      ],
      declarations: [ MonsterFiltersComponent, InputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonsterFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
