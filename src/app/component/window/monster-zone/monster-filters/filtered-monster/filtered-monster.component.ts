import { Component, Input, OnInit } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCircleDot } from '@fortawesome/free-solid-svg-icons';
import { MonsterDto } from 'src/app/dto/classes/monster-dto';
import { MonsterFiltersService } from 'src/app/service/monster-filters.service';

@Component({
  selector: 'app-filtered-monster',
  templateUrl: './filtered-monster.component.html',
  styleUrls: ['./filtered-monster.component.css']
})
export class FilteredMonsterComponent implements OnInit {

  @Input() monster?: MonsterDto;

  public isSelected: boolean = false;

  public circleIcon: IconDefinition = faCircleDot;

  constructor(private filterService: MonsterFiltersService) { 
    
  }

  ngOnInit(): void {
    this.updateSelected();
  }

  updateSelected(): void {
    this.isSelected = !this.filterService.forbiddenMonsters.includes(<number>this.monster?.dofusId);
  }

  onSelectIconClicked(): void {
    this.isSelected = !this.isSelected;
    if(this.isSelected === true) {
      this.filterService.tryRemoveForbiddenMonster(<number>this.monster?.dofusId);
    } else {
      this.filterService.tryAddForbiddenMonster(<number>this.monster?.dofusId);
    }
  }
}
