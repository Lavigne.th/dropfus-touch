import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MonsterDto } from 'src/app/dto/classes/monster-dto';
import { MonsterFiltersService } from 'src/app/service/monster-filters.service';

import { FilteredMonsterComponent } from './filtered-monster.component';

describe('FilteredMonsterComponent', () => {
  let component: FilteredMonsterComponent;
  let fixture: ComponentFixture<FilteredMonsterComponent>;
  let filterService: MonsterFiltersService;
  let monster: MonsterDto = new MonsterDto(1, 1, "test1", true, true, true);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MonsterFiltersService],
      declarations: [ FilteredMonsterComponent ]
    })
    .compileComponents();

    filterService = TestBed.inject(MonsterFiltersService);
    filterService.setMonsters([
      monster,
      new MonsterDto(2, 2, "test2", true, true, true)
    ]);

    fixture = TestBed.createComponent(FilteredMonsterComponent);
    component = fixture.componentInstance;
    component.monster = monster;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sould select and deselect itself', () => {
    expect(component.isSelected).toBeTrue();
    let spy = spyOn(filterService.filteredMonstersChangedEvent, 'next');
    //deselect itself
    component.onSelectIconClicked();
    expect(component.isSelected).toBeFalse();
    expect(filterService.filteredMonsters.includes(<MonsterDto>component.monster)).toBeTrue();
    expect(spy).toHaveBeenCalled();

    spy.calls.reset();
    //select itself
    component.onSelectIconClicked();
    expect(component.isSelected).toBeTrue();
    expect(filterService.filteredMonsters.includes(<MonsterDto>component.monster)).toBeTrue();
    expect(spy).toHaveBeenCalled();
  });
});
