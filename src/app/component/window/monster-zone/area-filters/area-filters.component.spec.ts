import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { InputComponent } from 'src/app/component/templates/input/input.component';
import { AreaFiltersService } from 'src/app/service/area-filters.service';

import { AreaFiltersComponent } from './area-filters.component';

describe('AreaFiltersComponent', () => {
  let component: AreaFiltersComponent;
  let fixture: ComponentFixture<AreaFiltersComponent>;
  let filterService: AreaFiltersService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule
      ],
      declarations: [ AreaFiltersComponent, InputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AreaFiltersComponent);
    component = fixture.componentInstance;
    filterService = TestBed.inject(AreaFiltersService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.areaNameInput !== undefined).toBeTrue();
  });

  it('should update area name filter and filter', () => {
    expect(filterService.areaNameFilter).toEqual('');
    (<InputComponent>component.areaNameInput).value = 'test1';
    (<InputComponent>component.areaNameInput).valueChange.emit('test1');
    
    fixture.detectChanges();
    expect(filterService.areaNameFilter).toEqual('test1')
  });
});
