import { Component, Input, OnInit } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCircleDot } from '@fortawesome/free-solid-svg-icons';
import { SubAreaDto } from 'src/app/dto/classes/sub-area-dto';
import { AreaFiltersService } from 'src/app/service/area-filters.service';

@Component({
  selector: 'app-filtered-area',
  templateUrl: './filtered-area.component.html',
  styleUrls: ['./filtered-area.component.css']
})
export class FilteredAreaComponent implements OnInit {

  @Input() area?: SubAreaDto;

  public isSelected: boolean = false;

  public circleIcon: IconDefinition = faCircleDot;

  constructor(private filterService: AreaFiltersService) { 
    
  }

  ngOnInit(): void {
    this.updateSelected();
  }

  updateSelected(): void {
    this.isSelected = !this.filterService.forbiddenAreas.includes(<number>this.area?.dofusId);
  }

  onSelectIconClicked(): void {
    this.isSelected = !this.isSelected;
    if(this.isSelected === true) {
      this.filterService.tryRemoveForbiddenArea(<number>this.area?.dofusId);
    } else {
      this.filterService.tryAddForbiddenArea(<number>this.area?.dofusId);
    }
  }

}
