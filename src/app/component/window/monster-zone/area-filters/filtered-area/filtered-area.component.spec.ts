import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SubAreaDto } from 'src/app/dto/classes/sub-area-dto';
import { AreaFiltersService } from 'src/app/service/area-filters.service';
import { FilteredAreaComponent } from './filtered-area.component';

describe('FilteredAreaComponent', () => {
  let component: FilteredAreaComponent;
  let fixture: ComponentFixture<FilteredAreaComponent>;
  let maFilterService: AreaFiltersService;
  let subArea: SubAreaDto = new SubAreaDto(1, 1, 1, "test1");

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [AreaFiltersService],
      declarations: [FilteredAreaComponent]
    })
      .compileComponents();

    maFilterService = TestBed.inject(AreaFiltersService);
    maFilterService.setAreas([
      subArea,
      new SubAreaDto(2, 2, 2, "test2")
    ]);

    fixture = TestBed.createComponent(FilteredAreaComponent);
    component = fixture.componentInstance;
    component.area = subArea;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sould select and deselect itself', () => {
    let nbBefore: number = maFilterService.nbFiltered;
    expect(component.isSelected).toBeTrue();
    let spy = spyOn(maFilterService.filteredAreasChangedEvent, 'next');
    //deselect itself
    component.onSelectIconClicked();
    expect(component.isSelected).toBeFalse();
    expect(maFilterService.forbiddenAreas.includes((<SubAreaDto>component.area).dofusId)).toBeTrue();
    expect(maFilterService.filteredAreas.includes(<SubAreaDto>component.area)).toBeTrue();
    expect(spy).toHaveBeenCalledTimes(1);

    spy.calls.reset();
    //select itself
    component.onSelectIconClicked();
    expect(component.isSelected).toBeTrue();
    expect(maFilterService.forbiddenAreas.includes((<SubAreaDto>component.area).dofusId)).toBeFalse();
    expect(maFilterService.filteredAreas.includes(<SubAreaDto>component.area)).toBeTrue();
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
