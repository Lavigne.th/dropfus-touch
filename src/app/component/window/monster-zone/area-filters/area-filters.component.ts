import { AfterContentInit, AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs';
import { InputComponent } from 'src/app/component/templates/input/input.component';
import { SubAreaDto } from 'src/app/dto/classes/sub-area-dto';
import { AreaFiltersService } from 'src/app/service/area-filters.service';
import { WebapiService } from 'src/app/service/webapi.service';
import { FilteredAreaComponent } from './filtered-area/filtered-area.component';

@Component({
  selector: '[app-zone-filters]',
  templateUrl: './area-filters.component.html',
  styleUrls: ['./area-filters.component.css']
})
export class AreaFiltersComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild("areaNameInput") areaNameInput?: InputComponent;

  private sub?: Subscription;

  @ViewChildren(FilteredAreaComponent) filteredAreas!: QueryList<FilteredAreaComponent>;

  constructor(public areaFilterService: AreaFiltersService) { }

  ngAfterViewInit(): void {
    if(this.areaNameInput) {
      this.areaNameInput.value = this.areaFilterService.areaNameFilter;
      this.areaNameInput.changeRef.detectChanges();
    }
    this.areaNameInput?.valueChange.subscribe((newValue: string) => {
      this.areaFilterService.setAreaNameFilter(newValue);
    });
  }

  ngOnInit(): void {
    this.areaFilterService.loadAreas();
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  selectAll(): void {
    this.areaFilterService.selectAll();
    this.filteredAreas.forEach((f: FilteredAreaComponent) => {
      f.updateSelected();
    })
  }

  deSelectAll(): void {
    this.areaFilterService.deSelectAll();
    this.filteredAreas.forEach((f: FilteredAreaComponent) => {
      f.updateSelected();
    })
  }

}
