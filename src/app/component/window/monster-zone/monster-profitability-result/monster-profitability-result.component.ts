import { Component, Input, OnInit } from '@angular/core';
import { MonsterDropDto } from 'src/app/dto/classes/monster-drop-dto';
import { MonsterProfitabilityDto } from 'src/app/dto/classes/monster-profitability-dto';
import { SubAreaDto } from 'src/app/dto/classes/sub-area-dto';
import { Utils } from 'src/app/helper/utils';
import { AreaFiltersService } from 'src/app/service/area-filters.service';

@Component({
  selector: 'app-monster-profitability-result',
  templateUrl: './monster-profitability-result.component.html',
  styleUrls: ['./monster-profitability-result.component.css']
})
export class MonsterProfitabilityResultComponent implements OnInit {

   @Input() monster?: MonsterProfitabilityDto;

  constructor(public areaFilters: AreaFiltersService) { }

  ngOnInit(): void {
  }

  getSubAreaName(subAreaId: number): string {
    let subArea: SubAreaDto | undefined = this.areaFilters.areas.find((a) => a.dofusId === subAreaId);
    if (subArea === undefined) {
      return 'Unknown sub area'
    }
    return subArea.name;
  }
}
