import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CleanIntegerPipe } from 'src/app/helper/pipes/clean-integer-pipe';

import { MonsterProfitabilityResultComponent } from './monster-profitability-result.component';

describe('MonsterProfitabilityResultComponent', () => {
  let component: MonsterProfitabilityResultComponent;
  let fixture: ComponentFixture<MonsterProfitabilityResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule
      ],
      declarations: [ MonsterProfitabilityResultComponent, CleanIntegerPipe ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonsterProfitabilityResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
