import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CheckboxComponent } from 'src/app/component/templates/checkbox/checkbox.component';
import { SelectComponent } from 'src/app/component/templates/select/select.component';
import { DROP_REQUEST_TYPE } from 'src/app/dto/enums/drop-request-type-enum';

import { GeneralFiltersComponent } from './general-filters.component';

describe('GeneralFiltersComponent', () => {
  let component: GeneralFiltersComponent;
  let fixture: ComponentFixture<GeneralFiltersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [GeneralFiltersComponent, CheckboxComponent, SelectComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(GeneralFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.filterService.dropRequestType).toEqual(DROP_REQUEST_TYPE.MONSTERS);
    expect(component.filterService.dropRequestType).toEqual(parseInt(<string>component.requestTypeSelect?.getSelectedItem()?.value));
    expect(component.filterService.includeBoss).toBeTrue();
    expect(component.filterService.includeMiniBoss).toBeTrue();
    expect(component.filterService.includeQuestMonster).toBeTrue();
    expect(component.filterService.includeBestHormone).toBeTrue();
    expect(component.filterService.includeHunterDrops).toBeTrue();
  });

  it('should swap filters boolean values', () => {
    //boss
    component.bossCb?.onClick();
    expect(component.filterService.includeBoss).toBeFalse();
    component.bossCb?.onClick();
    expect(component.filterService.includeBoss).toBeTrue();

    //mini boss
    component.miniBossCb?.onClick();
    expect(component.filterService.includeMiniBoss).toBeFalse();
    component.miniBossCb?.onClick();
    expect(component.filterService.includeMiniBoss).toBeTrue();

    //quest
    component.questCb?.onClick();
    expect(component.filterService.includeQuestMonster).toBeFalse();
    component.questCb?.onClick();
    expect(component.filterService.includeQuestMonster).toBeTrue();

    //hormone
    component.hormoneCb?.onClick();
    expect(component.filterService.includeBestHormone).toBeFalse();
    component.hormoneCb?.onClick();
    expect(component.filterService.includeBestHormone).toBeTrue();

    //hunter
    component.hunterCb?.onClick();
    expect(component.filterService.includeHunterDrops).toBeFalse();
    component.hunterCb?.onClick();
    expect(component.filterService.includeHunterDrops).toBeTrue();
  });

  it('should change DROP_REQUEST_TYPE', () => {
    component.requestTypeSelect?.onItemClick(1);
    expect(component.filterService.dropRequestType).toEqual(DROP_REQUEST_TYPE.AREAS);
    component.requestTypeSelect?.onItemClick(0);
    expect(component.filterService.dropRequestType).toEqual(DROP_REQUEST_TYPE.MONSTERS);
  });
});
