import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CheckboxComponent } from 'src/app/component/templates/checkbox/checkbox.component';
import { SelectComponent } from 'src/app/component/templates/select/select.component';
import { DROP_REQUEST_TYPE } from 'src/app/dto/enums/drop-request-type-enum';
import { GeneralFilterService } from 'src/app/service/general-filter.service';

@Component({
  selector: '[app-general-filters]',
  templateUrl: './general-filters.component.html',
  styleUrls: ['./general-filters.component.css']
})
export class GeneralFiltersComponent implements OnInit, AfterViewInit, OnDestroy {

  public DROP_REQUEST_TYPE_ENUM: typeof DROP_REQUEST_TYPE = DROP_REQUEST_TYPE;

  @ViewChild("bossCb") bossCb?: CheckboxComponent;

  @ViewChild("miniBossCb") miniBossCb?: CheckboxComponent;

  @ViewChild("questCb") questCb?: CheckboxComponent;

  @ViewChild("hormoneCb") hormoneCb?: CheckboxComponent;

  @ViewChild("hunterCb") hunterCb?: CheckboxComponent;

  @ViewChild("requestTypeSelect") requestTypeSelect?: SelectComponent;

  private requestTypeSelectSub?: Subscription;

  constructor(public filterService: GeneralFilterService) { }

  ngAfterViewInit(): void {
    this.requestTypeSelectSub = this.requestTypeSelect?.selectedOptionChanged.subscribe(() => {
      if(this.requestTypeSelect) {
        let selected: any = this.requestTypeSelect.getSelectedItem();
        this.filterService.dropRequestType = <DROP_REQUEST_TYPE>parseInt(selected.value);
      } else {
        new Error("No select for request type");
      }
    });
  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    this.requestTypeSelectSub?.unsubscribe();
  }

}
