import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

import { MonsterDropPreviewComponent } from './monster-drop-preview.component';

describe('MonsterDropPreviewComponent', () => {
  let component: MonsterDropPreviewComponent;
  let fixture: ComponentFixture<MonsterDropPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      declarations: [ MonsterDropPreviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonsterDropPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
