import { Component, Input, OnInit } from '@angular/core';
import { MonsterProfitabilityDto } from 'src/app/dto/classes/monster-profitability-dto';

@Component({
  selector: 'app-monster-drop-preview',
  templateUrl: './monster-drop-preview.component.html',
  styleUrls: ['./monster-drop-preview.component.css']
})
export class MonsterDropPreviewComponent implements OnInit {

  @Input() monster?: MonsterProfitabilityDto;

  constructor() { }

  ngOnInit(): void {
  }

}
