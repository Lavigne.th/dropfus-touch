import { Component, Input, OnInit } from '@angular/core';
import { SubAreaProfitabilityDto } from 'src/app/dto/classes/sub-area-profitability-dto';

@Component({
  selector: 'app-area-profitability-result',
  templateUrl: './area-profitability-result.component.html',
  styleUrls: ['./area-profitability-result.component.css']
})
export class AreaProfitabilityResultComponent implements OnInit {

  @Input() area?: SubAreaProfitabilityDto;

  constructor() { }

  ngOnInit(): void {
  }

}
