import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CleanIntegerPipe } from 'src/app/helper/pipes/clean-integer-pipe';

import { AreaProfitabilityResultComponent } from './area-profitability-result.component';

describe('AreaProfitabilityResultComponent', () => {
  let component: AreaProfitabilityResultComponent;
  let fixture: ComponentFixture<AreaProfitabilityResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreaProfitabilityResultComponent, CleanIntegerPipe ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AreaProfitabilityResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
