import { AfterContentInit, Component, HostListener, OnInit } from '@angular/core';
import { Utils } from 'src/app/helper/utils';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit, AfterContentInit {

  public maxContentHeight: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.processMaxHeight();
  }

  ngAfterContentInit(): void {
    this.processMaxHeight();
  }

    /**
   * Calcul la hauteur maximale du contenu afin que la fenetre tienne dans la page sans scroll
   */
     processMaxHeight() {
      let mainPadding: number = 0;
      let mainHeight: number = 0;
      try {
        let mainElement: HTMLElement = <HTMLElement>document.querySelector('main');
        mainPadding = parseInt(getComputedStyle(mainElement)['padding'].replace('px', ''));
        mainHeight = mainElement.offsetHeight
  
      } catch (e) {
        mainPadding = 0;
        mainHeight = 0;
      }
      //remToPixels: 3 estimated size of the title of the window, 1.5 2 * padding of the container
      this.maxContentHeight = mainHeight - 2 * mainPadding - Utils.remToPixels(3) - Utils.remToPixels(1.5);
    }

}
