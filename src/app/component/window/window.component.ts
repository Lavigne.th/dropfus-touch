import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterContentInit, ChangeDetectorRef, Component, HostListener, Input, OnInit } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { SCREEN_SIZE } from 'src/app/enums/screen-size-enum';
import { WINDOW } from 'src/app/enums/window-enum';
import { DESKTOP_WINDOW_WIDTH } from 'src/app/enums/window-width-enum';
import { MenuService } from 'src/app/service/menu.service';
import { WindowService } from 'src/app/service/window.service';

@Component({
  selector: 'app-window',
  templateUrl: './window.component.html',
  styleUrls: ['./window.component.css'],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        opacity: 0,
      })),
      transition('closed => open', [
        animate('0.2s')
      ]),
      transition('open => closed', [
        animate('0.2s')
      ]),
    ])
  ],
  providers: [
    WindowService
  ]
})
export class WindowComponent implements OnInit, AfterContentInit {

  @Input() windowWidth: DESKTOP_WINDOW_WIDTH = DESKTOP_WINDOW_WIDTH.FULL;

  @Input() window: WINDOW = WINDOW.NONE;

  @Input() title: string = '';

  @Input() scrollContent: boolean = true;

  public DESKTOP_WINDOW_WIDTH_ENUM = DESKTOP_WINDOW_WIDTH;

  public WINDOW_ENUM = WINDOW;

  public isOpen: boolean = false;

  public sub?: Subscription;

  public backIcon: IconDefinition = faArrowLeft;

  constructor(private menu: MenuService, public windowService: WindowService) { }
  
  ngAfterContentInit(): void {
    if (this.window === WINDOW.ABOUT) {
      this.menu.changeCurrentWindow(WINDOW.ABOUT);
    }
  }

  ngOnInit(): void {

    this.sub = this.menu.windowSubject.subscribe((window: WINDOW) => {
      if(window === this.window) {
        this.isOpen = true;
      } else {
        this.isOpen = false;
      }
    });
  }

  onCloseBtnClicked(): void {
    this.menu.changeCurrentWindow(WINDOW.NONE);
  }

}
