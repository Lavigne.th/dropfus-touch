import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { WINDOW } from 'src/app/enums/window-enum';
import { MenuService } from 'src/app/service/menu.service';

import { WindowComponent } from './window.component';

describe('WindowComponent', () => {
  let component: WindowComponent;
  let fixture: ComponentFixture<WindowComponent>;
  let menuService: MenuService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule
      ],
      declarations: [WindowComponent],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();

    fixture = TestBed.createComponent(WindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    menuService = TestBed.inject(MenuService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.isOpen).toBeFalse();
  });

  it('should open and close about window when current change in menu service', () => {
    component.window = WINDOW.ABOUT;

    menuService.changeCurrentWindow(WINDOW.CHART);
    expect(component.isOpen).toBeFalse();

    //open
    menuService.changeCurrentWindow(WINDOW.ABOUT);
    expect(component.isOpen).toBeTrue();

    //close
    menuService.changeCurrentWindow(WINDOW.CHART);
    expect(component.isOpen).toBeFalse();

    //open
    menuService.changeCurrentWindow(WINDOW.ABOUT);
    expect(component.isOpen).toBeTrue();

    //close
    component.onCloseBtnClicked();
    expect(component.isOpen).toBeFalse();
  });

  it('should open and close monster_zone window when current change in menu service', () => {
    component.window = WINDOW.MONSTER_ZONE;

    menuService.changeCurrentWindow(WINDOW.CHART);
    expect(component.isOpen).toBeFalse();

    //open
    menuService.changeCurrentWindow(WINDOW.MONSTER_ZONE);
    expect(component.isOpen).toBeTrue();

    //close
    menuService.changeCurrentWindow(WINDOW.CHART);
    expect(component.isOpen).toBeFalse();

    //open
    menuService.changeCurrentWindow(WINDOW.MONSTER_ZONE);
    expect(component.isOpen).toBeTrue();

    //close
    component.onCloseBtnClicked();
    expect(component.isOpen).toBeFalse();
  });

  it('should open and close charts window when current change in menu service', () => {
    component.window = WINDOW.CHART;
    menuService.changeCurrentWindow(WINDOW.MONSTER_ZONE);
    expect(component.isOpen).toBeFalse();

    //open
    menuService.changeCurrentWindow(WINDOW.CHART);
    expect(component.isOpen).toBeTrue();

    //close
    menuService.changeCurrentWindow(WINDOW.MONSTER_ZONE);
    expect(component.isOpen).toBeFalse();

    //open
    menuService.changeCurrentWindow(WINDOW.CHART);
    expect(component.isOpen).toBeTrue();

    //close
    component.onCloseBtnClicked();
    expect(component.isOpen).toBeFalse();
  });
});
