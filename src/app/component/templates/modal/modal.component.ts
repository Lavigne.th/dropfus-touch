import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterContentInit, AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { MODAL_POSITION } from 'src/app/enums/modal-position-enum';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css'],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        opacity: 0,
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ])
  ]
})
export class ModalComponent implements OnInit, AfterContentInit, AfterViewInit {

  @Input() position: MODAL_POSITION = MODAL_POSITION.STICKY_MIDDLE; 

  @Input() visible: boolean = false;

  @Input() content: string = '';

  @Input() title: string = '';

  @ViewChild('modalElement', { static: true }) modal?: ElementRef;

  public top: number = 0;

  isOpen = false;

  constructor(private detectorRef: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    this.isOpen = this.visible;
    this.detectorRef.detectChanges();
  }

  ngAfterContentInit(): void {
    this.processTop();
  }

  ngOnInit(): void {
    
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.processTop();

  }

  processTop(): void {
    if(this.modal !== undefined && this.position === MODAL_POSITION.STICKY_MIDDLE) {
      let top: number = window.innerHeight / 2 - this.modal?.nativeElement.offsetHeight / 2;
      if(top > 0) {
        this.top = top;
      }
    }
  }

  onOkBtnClicked(evt: MouseEvent): void {
    this.isOpen = false;
  }

  onCloseBtnClicked(evt: MouseEvent): void {
    this.isOpen = false;
  }
}
