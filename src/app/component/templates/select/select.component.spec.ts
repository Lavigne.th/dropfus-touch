import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SelectOption } from './select-option';

import { SelectComponent } from './select.component';

describe('SelectComponent', () => {
  let component: SelectComponent;
  let fixture: ComponentFixture<SelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;
    component.options.push(new SelectOption('Dog', '1'));
    component.options.push(new SelectOption('Cat', '2'));
    component.options.push(new SelectOption('T-rex', '5'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.getSelectedItem()).toBeNull();
    expect(component.getIndexes().length).toEqual(3);
    expect(component.getIndexes()[0]).toEqual(0);
    expect(component.getIndexes()[2]).toEqual(2);
  });

  it('should create', () => {
    let spy = spyOn(component.selectedOptionChanged, 'emit');
    component.onItemClick(2);
    expect(component.getSelectedItem()).toEqual(component.options[2]);
    expect(component.getSelectedItem()?.value).toEqual("5");
    expect(spy).toHaveBeenCalled();
    
    spy.calls.reset();
    component.onItemClick(0);
    expect(component.getSelectedItem()).toEqual(component.options[0]);
    expect(component.getSelectedItem()?.value).toEqual("1");
    expect(spy).toHaveBeenCalled();
  });
});
