import { ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faSquareCaretDown } from '@fortawesome/free-solid-svg-icons';
import { SelectOption } from './select-option';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  @Output() selectedOptionChanged: EventEmitter<any> = new EventEmitter();

  @Input() options: Array<SelectOption> = [];

  @Input() name: string = '';

  @Input() cssClass: string = '';

  @Input() selectedIndex: number = -1;

  public arrowIcon: IconDefinition = faSquareCaretDown;

  public isOpen: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  onItemClick(index: number): void {
    this.selectedIndex = index;
    this.selectedOptionChanged.emit();
  }

  getSelectedItem(): SelectOption | null {
    if(this.selectedIndex >= 0) {
      return this.options[this.selectedIndex];
    }
    return null;
  }

  getIndexes(): Array<number> {
    let indexes: Array<number> = [];
    for(let i = 0; i < this.options.length; i++) {
      indexes.push(i);
    }
    return indexes;
  }

  onDropDownClicked(mouseEvent: MouseEvent): void {
    this.isOpen = true;
    mouseEvent.stopPropagation();
  }

  @HostListener('document:click')
  clickout() {
    if (this.isOpen === true) {
      this.isOpen = false;
    }
  }
}
