import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() type: string = 'text';

  @Input() name: string = '';

  @Input() cssClasses?: string;

  @Input() value:  string = '';

  @Output() valueChange = new EventEmitter<string>();

  constructor(public changeRef: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

}
