import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { sleep } from 'src/app/helper/utils';

import { CheckboxComponent } from './checkbox.component';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    component.value = "test";
    component.name = "test"
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be able to be checked', async () => {
    expect(component.checked).toBeFalse();
    component.onClick();
    fixture.detectChanges();
    console.log(fixture.nativeElement.querySelector('input'))
    expect(fixture.nativeElement.querySelector('input').checked).toBeTrue();
    component.onClick();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('input').checked).toBeFalse();
  });
});
