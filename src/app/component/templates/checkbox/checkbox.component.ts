import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() name: string = '';

  @Input() value: string = '';

  @Input() checked: boolean = false;

  @Output() ckeckedChanged: EventEmitter<boolean> = new EventEmitter();

  public checkIcon: IconDefinition = faCheck;

  constructor() { }

  ngOnInit(): void {
  }

  onClick(): void {
    this.checked = !this.checked;
    this.ckeckedChanged.emit(this.checked);
  }

}
