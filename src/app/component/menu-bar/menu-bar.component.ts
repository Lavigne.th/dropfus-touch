import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { faChartLine, faK, faKipSign, faQuestion } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from '@ngx-translate/core';
import { Constant } from 'src/app/constant';
import { SUPPORTED_LANGUAGE } from 'src/app/enums/supported-language-enum';
import { WINDOW } from 'src/app/enums/window-enum';
import { LocalStorageService } from 'src/app/service/local-storage.service';
import { MenuService } from 'src/app/service/menu.service';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.css']
})
export class MenuBarComponent implements OnInit {

  public currentLanguage: SUPPORTED_LANGUAGE = SUPPORTED_LANGUAGE.FRENCH;

  public SUPPORTED_LANGUAGE_ENUM = SUPPORTED_LANGUAGE;

  public WINDOW_ENUM : typeof WINDOW = WINDOW;

  public isLangChoicesOpen: boolean = false;

  public moneyIcon = faK;

  public chartIcon = faChartLine;

  public aboutIcon = faQuestion;

  constructor(private translateService: TranslateService, private localStorage: LocalStorageService, private menuService: MenuService) { }

  ngOnInit(): void {
    this.currentLanguage = <SUPPORTED_LANGUAGE> this.translateService.currentLang;
  }

  onSetEnglishLanguage() {
    this.setLanguage(SUPPORTED_LANGUAGE.ENGLISH);
  }
  onSetFrenchLanguage() {
    this.setLanguage(SUPPORTED_LANGUAGE.FRENCH);
  }

  public setLanguage(lang: SUPPORTED_LANGUAGE): void {
    this.currentLanguage = lang;
    this.translateService.use(lang);
    this.localStorage.set(Constant.LOCAL_STORAGE_LANGUAGE_KEY, lang);
  }

  onMonsterIconClicked(): void {
    this.menuService.changeCurrentWindow(WINDOW.MONSTER_ZONE);
  }

  onChartIconClicked(): void {
    this.menuService.changeCurrentWindow(WINDOW.CHART);
  }

  onAboutIconClicked(): void {
    this.menuService.changeCurrentWindow(WINDOW.ABOUT);
  }

}
