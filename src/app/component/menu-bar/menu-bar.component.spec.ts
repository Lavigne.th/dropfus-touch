import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { Constant } from 'src/app/constant';
import { SUPPORTED_LANGUAGE } from 'src/app/enums/supported-language-enum';
import { WINDOW } from 'src/app/enums/window-enum';
import { LocalStorageService } from 'src/app/service/local-storage.service';
import { MenuService } from 'src/app/service/menu.service';

import { MenuBarComponent } from './menu-bar.component';

describe('MenuBarComponent', () => {
  let component: MenuBarComponent;
  let fixture: ComponentFixture<MenuBarComponent>;
  let localStorageService: LocalStorageService;
  let translateService: TranslateService;
  let menuService: MenuService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      declarations: [ MenuBarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MenuBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    localStorageService = TestBed.inject(LocalStorageService);
    translateService = TestBed.inject(TranslateService);
    menuService = TestBed?.inject(MenuService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change the langage to english', () => {
    component.onSetEnglishLanguage();
    expect(localStorageService.get(Constant.LOCAL_STORAGE_LANGUAGE_KEY)).toEqual(SUPPORTED_LANGUAGE.ENGLISH);
    expect(translateService.currentLang).toEqual(SUPPORTED_LANGUAGE.ENGLISH);
    expect(component.currentLanguage).toEqual(SUPPORTED_LANGUAGE.ENGLISH);
  });

  it('should change the langage to french', () => {
    component.onSetFrenchLanguage();
    expect(localStorageService.get(Constant.LOCAL_STORAGE_LANGUAGE_KEY)).toEqual(SUPPORTED_LANGUAGE.FRENCH);
    expect(translateService.currentLang).toEqual(SUPPORTED_LANGUAGE.FRENCH);
    expect(component.currentLanguage).toEqual(SUPPORTED_LANGUAGE.FRENCH);
  });

  it('should change current window to monster', () => {
    let sub = menuService.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.MONSTER_ZONE);
    });
    let spy = spyOn(menuService.windowSubject, 'next');
    component.onMonsterIconClicked();

    expect(spy).toHaveBeenCalled();
    expect(menuService.currentWindow).toEqual(WINDOW.MONSTER_ZONE);
    sub.unsubscribe();
  });

  it('should change current window to chart', () => {
    let sub = menuService.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.CHART);
    });
    let spy = spyOn(menuService.windowSubject, 'next');
    component.onChartIconClicked();

    expect(spy).toHaveBeenCalled();
    expect(menuService.currentWindow).toEqual(WINDOW.CHART);
    sub.unsubscribe();
  });

  it('should change current window to about', () => {
    let sub = menuService.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.ABOUT);
    });
    let spy = spyOn(menuService.windowSubject, 'next');
    component.onAboutIconClicked();

    expect(spy).toHaveBeenCalled();
    expect(menuService.currentWindow).toEqual(WINDOW.ABOUT);
    sub.unsubscribe();
  });
});
