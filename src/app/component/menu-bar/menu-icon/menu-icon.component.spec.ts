import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WINDOW } from 'src/app/enums/window-enum';
import { MenuService } from 'src/app/service/menu.service';

import { MenuIconComponent } from './menu-icon.component';

describe('MenuIconComponent', () => {
  let component: MenuIconComponent;
  let fixture: ComponentFixture<MenuIconComponent>;
  let menuService: MenuService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuIconComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MenuIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    menuService = TestBed.inject(MenuService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.isWindowOpened).toBeFalse();
  });

  it('should be linked with the window state', () => {
    component.linkedWindow = WINDOW.ABOUT;
    expect(component.isWindowOpened).toBeFalse();

    menuService.changeCurrentWindow(WINDOW.ABOUT);
    expect(component.isWindowOpened).toBeTrue();

    menuService.changeCurrentWindow(WINDOW.NONE);
    expect(component.isWindowOpened).toBeFalse();

  });
  
});
