import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { Subscription } from 'rxjs';
import { WINDOW } from 'src/app/enums/window-enum';
import { MenuService } from 'src/app/service/menu.service';

@Component({
  selector: 'app-menu-icon',
  templateUrl: './menu-icon.component.html',
  styleUrls: ['./menu-icon.component.css'],
  animations: [
    trigger('mouseOverIcon', [
      state('over', style({
        fontSize: '1.5rem',
      })),
      state('outside', style({
        fontSize: '1rem',
      })),
      transition('over => outside', [
        animate('0.4s ease-out')
      ]),
      transition('outside => over', [
        animate('0.4s ease-out')
      ]),
    ]),
  ]
})
export class MenuIconComponent implements OnInit {

  @Input() menuIcon?: IconDefinition;

  @Input() isSideMenu: boolean = false;

  @Input() linkedWindow: WINDOW = WINDOW.NONE;

  public isMouseOver: boolean = false;

  public isWindowOpened: boolean = false;

  public sub?: Subscription;

  constructor(private menuService: MenuService) { }

  ngOnInit(): void {
    this.sub = this.menuService.windowSubject.subscribe((window: WINDOW) => {
      if(window === this.linkedWindow) {
        this.isWindowOpened = true;
      } else {
        this.isWindowOpened = false;
      }
    });
  }
  
  onMouseEnter(): void {
    this.isMouseOver = true;
  }

  onMouseLeave(): void {
    this.isMouseOver = false;
  }

}
