export enum WINDOW {
    NONE = 'none',
    MONSTER_ZONE = 'monster_zone_window',
    CHART = 'chart_window',
    ABOUT = 'about'
}