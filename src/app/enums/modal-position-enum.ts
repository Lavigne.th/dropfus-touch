export enum MODAL_POSITION {
    STICKY_TOP = 'top',
    STICKY_MIDDLE = 'middle',
    STICKY_BOTTOM = 'bottom'
}