export enum SUPPORTED_LANGUAGE {
    FRENCH = 'fr',
    ENGLISH = 'en'
}