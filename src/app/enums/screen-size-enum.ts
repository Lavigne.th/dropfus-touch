export enum SCREEN_SIZE {
    SM = 480,
    MD = 768,
    LG = 976,
    XL = 1440
}