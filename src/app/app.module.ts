import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonComponent } from './component/templates/button/button.component';
import { ModalComponent } from './component/templates/modal/modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { IconBtnComponent } from './component/templates/icon-btn/icon-btn.component';
import { MenuBarComponent } from './component/menu-bar/menu-bar.component';
import { WindowComponent } from './component/window/window.component';
import { MenuIconComponent } from './component/menu-bar/menu-icon/menu-icon.component';
import { AboutComponent } from './component/window/about/about.component';
import { SkillRowComponent } from './component/window/about/skill-row/skill-row.component';
import { MonsterZoneComponent } from './component/window/monster-zone/monster-zone.component';
import { ChartsComponent } from './component/window/charts/charts.component';
import { InputComponent } from './component/templates/input/input.component';
import { CheckboxComponent } from './component/templates/checkbox/checkbox.component';
import { SelectComponent } from './component/templates/select/select.component';
import { TabHeaderDirective } from './directive/tab-header.directive';
import { GeneralFiltersComponent } from './component/window/monster-zone/general-filters/general-filters.component';
import { FilteredAreaComponent } from './component/window/monster-zone/area-filters/filtered-area/filtered-area.component';
import { AreaFiltersComponent } from './component/window/monster-zone/area-filters/area-filters.component';
import { FormsModule } from '@angular/forms';
import { MonsterFiltersComponent } from './component/window/monster-zone/monster-filters/monster-filters.component';
import { FilteredMonsterComponent } from './component/window/monster-zone/monster-filters/filtered-monster/filtered-monster.component';
import { LoaderComponent } from './component/templates/loader/loader.component';
import { MonsterProfitabilityResultComponent } from './component/window/monster-zone/monster-profitability-result/monster-profitability-result.component';
import { CleanIntegerPipe } from './helper/pipes/clean-integer-pipe';
import { AccordionDirective } from './directive/accordion.directive';
import { AreaProfitabilityResultComponent } from './component/window/monster-zone/area-profitability-result/area-profitability-result.component';
import { MonsterDropPreviewComponent } from './component/window/monster-zone/monster-drop-preview/monster-drop-preview.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    ModalComponent,
    IconBtnComponent,
    MenuBarComponent,
    WindowComponent,
    MenuIconComponent,
    AboutComponent,
    SkillRowComponent,
    MonsterZoneComponent,
    ChartsComponent,
    InputComponent,
    CheckboxComponent,
    SelectComponent,
    TabHeaderDirective,
    GeneralFiltersComponent,
    AreaFiltersComponent,
    FilteredAreaComponent,
    MonsterFiltersComponent,
    FilteredMonsterComponent,
    LoaderComponent,
    MonsterProfitabilityResultComponent,
    CleanIntegerPipe,
    AccordionDirective,
    AreaProfitabilityResultComponent,
    MonsterDropPreviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    TranslateModule.forRoot({
      defaultLanguage: 'fr',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
    }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
