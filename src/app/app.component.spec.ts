import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { Constant } from './constant';
import { SUPPORTED_LANGUAGE } from './enums/supported-language-enum';
import { LocalStorageService } from './service/local-storage.service';

describe('AppComponent', () => {
  let localStorageService: LocalStorageService;
  let translateService: TranslateService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [
        AppComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();

    localStorageService = TestBed.inject(LocalStorageService);
    translateService = TestBed.inject(TranslateService);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`to set current language depending on the localStorage'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;

    localStorageService.unset(Constant.LOCAL_STORAGE_LANGUAGE_KEY);
    fixture.componentInstance.setCurrentLangage();
    expect(translateService.currentLang).toEqual(SUPPORTED_LANGUAGE.FRENCH);

    localStorageService.set(Constant.LOCAL_STORAGE_LANGUAGE_KEY, SUPPORTED_LANGUAGE.ENGLISH);
    fixture.componentInstance.setCurrentLangage();
    expect(translateService.currentLang).toEqual(SUPPORTED_LANGUAGE.ENGLISH);

    localStorageService.set(Constant.LOCAL_STORAGE_LANGUAGE_KEY, SUPPORTED_LANGUAGE.FRENCH);
    fixture.componentInstance.setCurrentLangage();
    expect(translateService.currentLang).toEqual(SUPPORTED_LANGUAGE.FRENCH);
  });
});
