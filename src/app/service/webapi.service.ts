import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constant } from '../constant';
import { MonsterAreaDropRequest } from '../dto/request/monster-area-drop-request';
import { AllMonsterResponse } from '../dto/response/all-monster-response';
import { AllSubAreaResponse } from '../dto/response/all-sub-area-response';
import { AreaDropResponse } from '../dto/response/areas-drop-response';
import { MonstersDropResponse } from '../dto/response/monsters-drop-response';

@Injectable({
  providedIn: 'root'
})
export class WebapiService {

  constructor(private http: HttpClient) { 
    
  }

  public getAllMonster(): Observable<AllMonsterResponse> {
    return this.http.get<AllMonsterResponse>(Constant.WEB_API_IP + "/monster/all");
  }

  public getAllSubArea(): Observable<AllSubAreaResponse> {
    return this.http.get<AllSubAreaResponse>(Constant.WEB_API_IP + "/sub-area/all");
  }

  public getMonsterDrops(request: MonsterAreaDropRequest): Observable<MonstersDropResponse> {
    return this.http.post<MonstersDropResponse>(Constant.WEB_API_IP + "/monster/drops", request);
  }

  public getSubAreaDrops(request: MonsterAreaDropRequest): Observable<AreaDropResponse> {
    return this.http.post<AreaDropResponse>(Constant.WEB_API_IP + "/sub-area/drops", request);
  }
}
