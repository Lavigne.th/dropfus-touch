import { TestBed } from '@angular/core/testing';

import { LocalStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set and get values', () => {
    service.set('hello', 'world');
    expect(service.get('hello')).toEqual('world');

    service.set('hello', 'my friend');
    expect(service.get('hello')).toEqual('my friend');

    service.set('hello !', 'world');
    expect(service.get('hello !')).toEqual('world');
  });

  it('sould unset values', () => {
    service.set('hello', 'world');
    expect(service.get('hello')).toBeDefined();
    service.unset('hello');
    expect(service.get('hello')).toBeNull();

    service.set('hello !', 'world');
    expect(service.get('hello !')).toBeDefined();
    service.unset('hello !');
    expect(service.get('hello !')).toBeNull();
  });
});
