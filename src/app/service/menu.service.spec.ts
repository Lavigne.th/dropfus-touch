import { TestBed } from '@angular/core/testing';
import { Observable, Subscription } from 'rxjs';
import { WINDOW } from '../enums/window-enum';

import { MenuService } from './menu.service';

describe('MenuService', () => {
  let service: MenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service.currentWindow).toEqual(WINDOW.NONE);
  });

  it('should change current window to ABOUT', () => {
    let sub: Subscription = service.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.ABOUT);
    });
    let spy = spyOn(service.windowSubject, 'next');
    service.changeCurrentWindow(WINDOW.ABOUT);
    expect(spy).toHaveBeenCalled();
    expect(service.currentWindow).toEqual(WINDOW.ABOUT);
    sub.unsubscribe();
  });

  it('should change current window to MONSTER', () => {
    let sub: Subscription = service.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.MONSTER_ZONE);
    });
    let spy = spyOn(service.windowSubject, 'next');
    service.changeCurrentWindow(WINDOW.MONSTER_ZONE);
    expect(spy).toHaveBeenCalled();
    expect(service.currentWindow).toEqual(WINDOW.MONSTER_ZONE);
    sub.unsubscribe();
  });

  it('should change current window to CHART', () => {
    let sub: Subscription = service.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.CHART);
    });
    let spy = spyOn(service.windowSubject, 'next');
    service.changeCurrentWindow(WINDOW.CHART);
    expect(spy).toHaveBeenCalled();
    expect(service.currentWindow).toEqual(WINDOW.CHART);
    sub.unsubscribe();
  });

  it('should change current window to NONE', () => {
    let sub: Subscription = service.windowSubject.subscribe((window: WINDOW) => {
      expect(window).toEqual(WINDOW.NONE);
    });
    let spy = spyOn(service.windowSubject, 'next');
    service.changeCurrentWindow(WINDOW.NONE);
    expect(spy).toHaveBeenCalled();
    expect(service.currentWindow).toEqual(WINDOW.NONE);
    sub.unsubscribe();
  });
});
