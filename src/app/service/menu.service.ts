import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { WINDOW } from '../enums/window-enum';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public windowSubject: Subject<WINDOW> = new Subject();

  public currentWindow: WINDOW = WINDOW.NONE;

  constructor() { 
    
  }

  changeCurrentWindow(window: WINDOW): void{
    this.currentWindow = window;
    this.windowSubject.next(window);
  }
}
