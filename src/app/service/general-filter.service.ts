import { Injectable } from '@angular/core';
import { DROP_REQUEST_TYPE } from '../dto/enums/drop-request-type-enum';

@Injectable({
  providedIn: 'root'
})
export class GeneralFilterService {

  public dropRequestType: DROP_REQUEST_TYPE = DROP_REQUEST_TYPE.MONSTERS;

  public includeBoss: boolean = true;

  public includeMiniBoss: boolean = true;

  public includeQuestMonster: boolean = true;

  public includeBestHormone: boolean = true;

  public includeHunterDrops: boolean = true;

  constructor() { }
}
