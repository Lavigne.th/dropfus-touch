import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { SubAreaDto } from '../dto/classes/sub-area-dto';
import { AllSubAreaResponse } from '../dto/response/all-sub-area-response';
import { WebapiService } from './webapi.service';

@Injectable({
  providedIn: 'root'
})
export class AreaFiltersService {

  public filteredAreas: Array<SubAreaDto> = [];

  public areas: Array<SubAreaDto> = [];

  public nbFiltered: number = 0;

  public forbiddenAreas: Array<number> = [];

  public areaNameFilter: string = '';

  public filteredAreasChangedEvent: Subject<any> = new Subject();

  constructor(private webApi: WebapiService) {  
    
  }

  public loadAreas(): void {
    this.webApi.getAllSubArea().subscribe((response: AllSubAreaResponse) => {
      this.setAreas(response.areas);
    })
  }

  public setAreaNameFilter(newName: string): void {
    this.areaNameFilter =  newName.toLowerCase();
    this.updateFilteredAreas();
  }

  setAreas(areas: Array<SubAreaDto>): void {
    this.areas = areas;
    this.updateFilteredAreas();
  }

  updateFilteredAreas() {
    let subAreas: Array<SubAreaDto> = this.areas.slice(0);
    if(this.areaNameFilter.length > 0) {
      subAreas = subAreas.filter((s) => s.name.toLowerCase().includes(this.areaNameFilter));
    }
    // this.filteredAreas = subAreas.filter((s) => this.forbiddenAreas.includes(s.dofusId) === false);
    this.filteredAreas = subAreas;
    this.nbFiltered = this.filteredAreas.length;
    this.filteredAreasChangedEvent.next(null);
  }

  tryAddForbiddenArea(subAreaDofusId: number): boolean {
    if(this.forbiddenAreas.includes(subAreaDofusId) === true) {
      new Error(`No forbidden sunarea for id ${subAreaDofusId}`);
      return false;
    }
    this.forbiddenAreas.push(subAreaDofusId);
    this.updateFilteredAreas();
    return true;    
  }

  tryRemoveForbiddenArea(subAreaDofusId: number): boolean {
    let index: number = this.forbiddenAreas.indexOf(subAreaDofusId);
    if(index < 0) {
      new Error(`No forbidden sunarea for id ${subAreaDofusId}`);
      return false;
    }
    this.forbiddenAreas.splice(index, 1);
    this.updateFilteredAreas();
    return true;    
  }

  selectAll(): void {
    this.forbiddenAreas = [];
    this.updateFilteredAreas();
  }

  deSelectAll(): void {
    this.forbiddenAreas = [];
    for(let area of this.areas) {
      this.forbiddenAreas.push(area.dofusId)
    }
    this.updateFilteredAreas();
  }
}
