import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { MonsterProfitabilityDto } from '../dto/classes/monster-profitability-dto';
import { SubAreaProfitabilityDto } from '../dto/classes/sub-area-profitability-dto';
import { DROP_REQUEST_TYPE } from '../dto/enums/drop-request-type-enum';
import { MonsterAreaDropRequest } from '../dto/request/monster-area-drop-request';
import { AreaDropResponse } from '../dto/response/areas-drop-response';
import { MonstersDropResponse } from '../dto/response/monsters-drop-response';
import { AreaFiltersService } from './area-filters.service';
import { GeneralFilterService } from './general-filter.service';
import { MonsterFiltersService } from './monster-filters.service';
import { WebapiService } from './webapi.service';

@Injectable({
  providedIn: 'root'
})
export class MonsterAreaDropResultService {

  public isLoading: boolean = false;

  public isLoadingPage: boolean = false; 

  public areaResults: Array<SubAreaProfitabilityDto> = [];

  public monsterResults: Array<MonsterProfitabilityDto> = [];

  public currentRequest: MonsterAreaDropRequest | null = null;

  public hasNextPage: boolean = false;

  constructor(private generalFilters: GeneralFilterService, private areaFilters: AreaFiltersService, private monsterFilters: MonsterFiltersService, private webapi: WebapiService) {

  }

  public generateMonsterAreaDropRequest(): MonsterAreaDropRequest {
    return new MonsterAreaDropRequest(
      this.generalFilters.dropRequestType,
      this.generalFilters.includeBoss,
      this.generalFilters.includeMiniBoss,
      this.generalFilters.includeQuestMonster,
      this.generalFilters.includeBestHormone,
      this.generalFilters.includeHunterDrops,
      this.areaFilters.forbiddenAreas,
      this.monsterFilters.forbiddenMonsters,
      0
    );
  }

  getDrops() {
    this.hasNextPage = false;
    this.areaResults = [];
    this.monsterResults = [];
    this.isLoading = true;
    this.isLoadingPage = false;
    this.currentRequest = this.generateMonsterAreaDropRequest();
    if(this.currentRequest.dropType === DROP_REQUEST_TYPE.MONSTERS) {
      this.webapi.getMonsterDrops(this.currentRequest).subscribe((response: MonstersDropResponse) => {
        this.isLoading = false;
        console.log(response);
        this.hasNextPage = response.totalResults > response.currentTotalResuls;
        this.monsterResults.push(...response.monsters);
      });
    } else if(this.currentRequest.dropType === DROP_REQUEST_TYPE.AREAS) {
      this.webapi.getSubAreaDrops(this.currentRequest).subscribe((response: AreaDropResponse) => {
        this.isLoading = false;
        this.hasNextPage = response.totalResults > response.currentTotalResuls;
        this.areaResults.push(...response.areas);
      });
    }
  }

  loadNextPage(): void {
    if(this.currentRequest !== null && this.hasNextPage === true) {
      this.isLoadingPage = true;
      this.currentRequest.page++;
      if(this.currentRequest.dropType === DROP_REQUEST_TYPE.MONSTERS) {
        this.webapi.getMonsterDrops(this.currentRequest).subscribe((response: MonstersDropResponse) => {
          this.isLoadingPage = false;
          this.hasNextPage = response.totalResults > response.currentTotalResuls;
          this.monsterResults.push(...response.monsters);
        });
      } else if(this.currentRequest.dropType === DROP_REQUEST_TYPE.AREAS) {
        this.webapi.getSubAreaDrops(this.currentRequest).subscribe((response: AreaDropResponse) => {
          this.isLoadingPage = false;
          this.hasNextPage = response.totalResults > response.currentTotalResuls;
          this.areaResults.push(...response.areas);
        });
      }
    }
  }

  public hasResults(): boolean {
    return this.currentRequest !== null && (this.areaResults.length > 0 || this.monsterResults.length > 0);
  }

}
