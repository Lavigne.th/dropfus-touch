import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WindowService {

  public showBackBtn: boolean = false;

  public backButtonClicked: Subject<any> = new Subject();

  constructor() { }
}
