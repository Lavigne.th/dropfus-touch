import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MonsterDto } from '../dto/classes/monster-dto';
import { AllMonsterResponse } from '../dto/response/all-monster-response';
import { WebapiService } from './webapi.service';

@Injectable({
  providedIn: 'root'
})
export class MonsterFiltersService {

  public filteredMonsters: Array<MonsterDto> = [];

  public monsters: Array<MonsterDto> = [];

  public nbFiltered: number = 0;

  public forbiddenMonsters: Array<number> = [];

  public monsterNameFilter: string = '';

  public filteredMonstersChangedEvent: Subject<any> = new Subject();

  constructor(private webApi: WebapiService) {  
    
  }

  public loadMonsters(): void {
    this.webApi.getAllMonster().subscribe((response: AllMonsterResponse) => {
      this.setMonsters(response.monsters);
    })
  }

  public setAreaNameFilter(newName: string): void {
    this.monsterNameFilter =  newName.toLowerCase();
    this.updateFilteredMonsters();
  }

  setMonsters(monsters: Array<MonsterDto>): void {
    this.monsters = monsters;
    this.updateFilteredMonsters();
  }

  updateFilteredMonsters() {
    let monsters: Array<MonsterDto> = this.monsters.slice(0);
    if(this.monsterNameFilter.length > 0) {
      monsters = monsters.filter((s) => s.name.toLowerCase().includes(this.monsterNameFilter));
    }
    //monsters = monsters.filter((s) => this.forbiddenMonsters.includes(s.dofusId) === false);
    this.filteredMonsters = monsters;
    this.nbFiltered = this.filteredMonsters.length;
    this.filteredMonstersChangedEvent.next(null);
  }

  tryAddForbiddenMonster(monsterDofusId: number): boolean {
    if(this.forbiddenMonsters.includes(monsterDofusId) === true) {
      new Error(`No forbidden monster for id ${monsterDofusId}`);
      return false;
    }
    this.forbiddenMonsters.push(monsterDofusId);
    this.updateFilteredMonsters();
    return true;    
  }

  tryRemoveForbiddenMonster(monsterDofusId: number): boolean {
    let index: number = this.forbiddenMonsters.indexOf(monsterDofusId);
    if(index < 0) {
      new Error(`No forbidden sunarea for id ${monsterDofusId}`);
      return false;
    }
    this.forbiddenMonsters.splice(index, 1);
    this.updateFilteredMonsters();
    return true;    
  }

  selectAll(): void {
    this.forbiddenMonsters = [];
    this.updateFilteredMonsters();
  }

  deSelectAll(): void {
    this.forbiddenMonsters = [];
    for(let monster of this.monsters) {
      this.forbiddenMonsters.push(monster.dofusId)
    }
    this.updateFilteredMonsters();
  }
}
