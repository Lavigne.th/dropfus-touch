import { TestBed } from '@angular/core/testing';

import { GeneralFilterService } from './general-filter.service';

describe('GeneralFilterService', () => {
  let service: GeneralFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeneralFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
