import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { SubAreaDto } from '../dto/classes/sub-area-dto';

import { AreaFiltersService } from './area-filters.service';

describe('AreaFiltersService', () => {
  let service: AreaFiltersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      schemas:[NO_ERRORS_SCHEMA]
    });
    service = TestBed.inject(AreaFiltersService);
    service.setAreas([
      new SubAreaDto(1, 1, 1, 'test1'),
      new SubAreaDto(1, 2, 1, 'test2'),
      new SubAreaDto(1, 3, 1, 'sub1'),
      new SubAreaDto(1, 4, 1, 'sub2'),
    ]);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service.areas.length).toEqual(4);
    expect(service.filteredAreas.length).toEqual(4);
    expect(service.nbFiltered).toEqual(4);
  });

  it('filter by name', () => {
    let spy = spyOn(service.filteredAreasChangedEvent, 'next');
    service.setAreaNameFilter('tEst');
    expect(service.areas.length).toEqual(4);
    expect(service.filteredAreas.length).toEqual(2);
    expect(service.filteredAreas.includes(service.areas[0])).toBeTrue()
    expect(service.filteredAreas.includes(service.areas[1])).toBeTrue()
    expect(spy).toHaveBeenCalled()

    spy.calls.reset();
    service.setAreaNameFilter('S');
    expect(service.areas.length).toEqual(4);
    expect(service.filteredAreas.length).toEqual(4);
    expect(spy).toHaveBeenCalled()

    spy.calls.reset();
    service.setAreaNameFilter('So');
    expect(service.areas.length).toEqual(4);
    expect(service.filteredAreas.length).toEqual(0);
    expect(spy).toHaveBeenCalled()
  });

  //deselected will be displayed for now
  it('should select all', () => {
    service.selectAll();
    expect(service.forbiddenAreas.length).toEqual(0);
  });

  it('should deselect all', () => {
    service.deSelectAll();
    expect(service.forbiddenAreas.length).toEqual(service.areas.length);
    for(let area of service.areas) {
      expect(service.forbiddenAreas.includes(area.dofusId)).toBeTrue();
    }
  });
});
