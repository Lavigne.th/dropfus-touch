import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { DROP_REQUEST_TYPE } from '../dto/enums/drop-request-type-enum';
import { MonsterAreaDropRequest } from '../dto/request/monster-area-drop-request';
import { AreaFiltersService } from './area-filters.service';
import { GeneralFilterService } from './general-filter.service';

import { MonsterAreaDropResultService } from './monster-area-drop-result.service';
import { MonsterFiltersService } from './monster-filters.service';
import { WebapiService } from './webapi.service';

describe('MonsterAreaDropResultService', () => {
  let service: MonsterAreaDropResultService;
  let areaFilter: AreaFiltersService;
  let monsterFilter: MonsterFiltersService;
  let generalFilter: GeneralFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        AreaFiltersService,
        MonsterFiltersService,
        GeneralFilterService,
        WebapiService
      ]
    });
    service = TestBed.inject(MonsterAreaDropResultService);
    areaFilter = TestBed.inject(AreaFiltersService);
    monsterFilter = TestBed.inject(MonsterFiltersService);
    generalFilter = TestBed.inject(GeneralFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create MonsterAreaDropRequest', () => {
    let request: MonsterAreaDropRequest = service.generateMonsterAreaDropRequest();
    expect(request.dropType).toEqual(DROP_REQUEST_TYPE.MONSTERS);
    expect(request.includeBosses).toEqual(true);
    expect(request.includeMiniBosses).toEqual(true);
    expect(request.includeQuestMonster).toEqual(true);
    expect(request.includeBestHormonePrice).toEqual(true);
    expect(request.includeHunterDrops).toEqual(true);
    expect(request.forbiddenSubAreas.length).toEqual(0);
    expect(request.forbiddenMonsters.length).toEqual(0);

    areaFilter.tryAddForbiddenArea(1);
    areaFilter.tryAddForbiddenArea(10);
    monsterFilter.tryAddForbiddenMonster(99);
    monsterFilter.tryAddForbiddenMonster(100);
    monsterFilter.tryAddForbiddenMonster(101);
    generalFilter.includeBoss = false;
    generalFilter.includeMiniBoss = false;
    generalFilter.includeQuestMonster = false;
    generalFilter.includeBestHormone = false;
    generalFilter.includeHunterDrops = false;

    request = service.generateMonsterAreaDropRequest();
    expect(request.dropType).toEqual(DROP_REQUEST_TYPE.MONSTERS);
    expect(request.includeBosses).toBeFalse();
    expect(request.includeMiniBosses).toBeFalse();
    expect(request.includeQuestMonster).toBeFalse();
    expect(request.includeBestHormonePrice).toBeFalse();
    expect(request.includeHunterDrops).toBeFalse();
    expect(request.forbiddenSubAreas.length).toEqual(2);
    expect(request.forbiddenSubAreas.includes(1)).toBeTrue();
    expect(request.forbiddenSubAreas.includes(10)).toBeTrue();
    expect(request.forbiddenMonsters.length).toEqual(3);
    expect(request.forbiddenMonsters.includes(99)).toBeTrue();
    expect(request.forbiddenMonsters.includes(100)).toBeTrue();
    expect(request.forbiddenMonsters.includes(101)).toBeTrue();
  });
});
