import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MonsterDto } from '../dto/classes/monster-dto';

import { MonsterFiltersService } from './monster-filters.service';

describe('MonsterFiltersService', () => {
  let service: MonsterFiltersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MonsterFiltersService);
    service.setMonsters([
      new MonsterDto(1, 1, 'test1', true, true, true),
      new MonsterDto(1, 2, 'test2', true, true, true),
      new MonsterDto(1, 3, 'sub1', true, true, true),
      new MonsterDto(1, 4, 'sub2', true, true, true),
    ]);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    expect(service.monsters.length).toEqual(4);
    expect(service.filteredMonsters.length).toEqual(4);
    expect(service.nbFiltered).toEqual(4);
  });

  it('should select all', () => {
    service.selectAll();
    expect(service.forbiddenMonsters.length).toEqual(0);
  });

  it('should deselect all', () => {
    service.deSelectAll();
    expect(service.forbiddenMonsters.length).toEqual(service.forbiddenMonsters.length);
    for(let monster of service.monsters) {
      expect(service.forbiddenMonsters.includes(monster.dofusId)).toBeTrue();
    }
  });
});
