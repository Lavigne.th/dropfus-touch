describe('Test of the side / bottom menu', () => {
  before(() => {
    localStorage.removeItem('lang');
    cy.visit('/')
  })

  it('should change language to english', () => {
    cy.get('[data-cy-role="english-lang"]').should('be.hidden');
    cy.get('[data-cy-role="french-lang"]').should('be.visible');
    cy.get('[data-cy-role="french-lang"]').click();
    cy.get('[data-cy-role="french-lang"]').should('be.hidden');
    cy.get('[data-cy-role="english-lang"]').should('be.visible');
  });

  it('should change language to french', () => {
    cy.get('[data-cy-role="french-lang"]').should('be.hidden');
    cy.get('[data-cy-role="english-lang"]').should('be.visible');
    cy.get('[data-cy-role="english-lang"]').click();
    cy.get('[data-cy-role="english-lang"]').should('be.hidden');
    cy.get('[data-cy-role="french-lang"]').should('be.visible');
  });

  it('should open / close monster', () => {
    cy.get('[data-cy-role="open-monster-window-btn"]').first().click();
    cy.wait(1000);
    cy.get('[data-cy-role="open-monster-window-btn"]').children().first().should('have.class', 'border-2');
    // cy.get('[data-cy-role="open-charts-window-btn"]').children().first().should('not.have.class', 'border-2');
    cy.get('[data-cy-role="open-about-window-btn"]').children().first().should('not.have.class', 'border-2');

    cy.get('[data-cy-role="monster-window"]').children().first().should('have.css', 'opacity', '1');
    cy.get('[data-cy-role="charts-window"]').children().first().should('have.css', 'opacity', '0');
    cy.get('[data-cy-role="about-window"]').children().first().should('have.css', 'opacity', '0');

    cy.get('[data-cy-role="monster-window"] [data-cy-role="close-window-btn"').click();
    cy.wait(200);
    cy.get('[data-cy-role="open-monster-window-btn"]').children().first().should('not.have.class', 'border-2');
    cy.get('[data-cy-role="monster-window"]').children().first().should('have.css', 'opacity', '0');

  });

  // it('should open / close charts', () => {
  //   cy.get('[data-cy-role="open-charts-window-btn"]').first().click();
  //   cy.wait(1000);
  //   cy.get('[data-cy-role="open-charts-window-btn"]').children().first().should('have.class', 'border-2');
  //   cy.get('[data-cy-role="open-monster-window-btn"]').children().first().should('not.have.class', 'border-2');
  //   cy.get('[data-cy-role="open-about-window-btn"]').children().first().should('not.have.class', 'border-2');

  //   cy.get('[data-cy-role="charts-window"]').children().first().should('have.css', 'opacity', '1');
  //   cy.get('[data-cy-role="monster-window"]').children().first().should('have.css', 'opacity', '0');
  //   cy.get('[data-cy-role="about-window"]').children().first().should('have.css', 'opacity', '0');

  //   cy.get('[data-cy-role="charts-window"] [data-cy-role="close-window-btn"').click();
  //   cy.wait(200);
  //   cy.get('[data-cy-role="open-charts-window-btn"]').children().first().should('not.have.class', 'border-2');
  //   cy.get('[data-cy-role="charts-window"]').children().first().should('have.css', 'opacity', '0');
  // });

  it('should open / close about', () => {
    cy.get('[data-cy-role="open-about-window-btn"]').first().click();
    cy.wait(1000);
    cy.get('[data-cy-role="open-about-window-btn"]').children().first().should('have.class', 'border-2');
    cy.get('[data-cy-role="open-monster-window-btn"]').children().first().should('not.have.class', 'border-2');
    // cy.get('[data-cy-role="open-charts-window-btn"]').children().first().should('not.have.class', 'border-2');

    cy.get('[data-cy-role="about-window"]').children().first().should('have.css', 'opacity', '1');
    cy.get('[data-cy-role="monster-window"]').children().first().should('have.css', 'opacity', '0');
    cy.get('[data-cy-role="charts-window"]').children().first().should('have.css', 'opacity', '0');
    
    cy.get('[data-cy-role="about-window"] [data-cy-role="close-window-btn"').click();
    cy.wait(200);
    cy.get('[data-cy-role="open-about-window-btn"]').children().first().should('not.have.class', 'border-2');
    cy.get('[data-cy-role="about-window"]').children().first().should('have.css', 'opacity', '0');
  });
})
