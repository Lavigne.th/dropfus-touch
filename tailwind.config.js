module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px'
    },
    extend: {
      colors: {
        lightGreen: '#cff64f',
        darkGreen: '#a4d738',
        //inventory
        contentBackground: '#3b3934',
        contentBorder: '#414137',
        contentFocus: '#59554c',
        inputFocus: '#635e53',
        menuIconBackground: '#33302d',
        accordionBackground: '#33302d',
        textRed: "#dc2626",
        //list
        itemLight: '#63615a',
        itemDark: '#4f4e49',
        itemRed: '#e33030',

        btnBorder: '#555e29',
        textGrey: '#4d600d',
        titleBar: '#1c1b19',
      }
    },
  },
  plugins: [],
}